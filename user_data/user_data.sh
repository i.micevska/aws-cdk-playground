#!/bin/bash

apt-get update
apt-get install -y nginx
ufw allow 'Nginx HTTP'
cp /home/ubuntu/proxy.com /etc/nginx/sites-enabled/proxy.com
rm /etc/nginx/sites-enabled/default
systemctl restart nginx
