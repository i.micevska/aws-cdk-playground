from aws_cdk import (
    core,
    aws_ec2 as ec2,
    aws_iam as iam,
    aws_s3 as s3
)

EC2_INSTANCE_TYPE = "t2.micro"
LINUX_AMI = ec2.AmazonLinuxImage()
KEY_NAME = "id_rsa"

with open("./user_data/user_data.sh") as f:
    data = f.read()
    USER_DATA=ec2.UserData
    USER_DATA.add_commands(str(data, 'utf-8'))

class AwsCdkPlaygroundStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # The code that defines your stack goes here

        bucket = s3.Bucket(self, "s3-breach-bucket", 
                        access_control=s3.BucketAccessControl.PRIVATE,
                        block_public_access=s3.BlockPublicAccess.BLOCK_ALL,
                        bucket_name="s3-breach-bucket")

        role = iam.Role(self, "s3-breach-role", 
                        assumed_by=iam.ServicePrincipal(service="ec2.amazonaws.com"))

        # IAM Role Policy Attachment
        role.add_to_policy(statement=iam.PolicyStatement(
                        effect=iam.Effect.ALLOW,
                        resources=["*"],
                        actions=["s3:*"])
        )
        
        vpc = ec2.Vpc(self, "s3-breach-vpc")

        host = ec2.Instance(self, "ec2-breach-instance", 
                        instance_type=ec2.InstanceType(instance_type_identifier=EC2_INSTANCE_TYPE),
                        instance_name="ec2-breach-instance",
                        machine_image=LINUX_AMI,
                        vpc=vpc,
                        key_name=KEY_NAME,
                        user_data=USER_DATA


        )

        




