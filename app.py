#!/usr/bin/env python3

from aws_cdk import core

from aws_cdk_playground.aws_cdk_playground_stack import AwsCdkPlaygroundStack


app = core.App()
AwsCdkPlaygroundStack(app, "aws-cdk-playground")

app.synth()
